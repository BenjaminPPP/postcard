package fr.eni.cardpostalproject;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.util.List;

import fr.eni.cardpostalproject.bo.Media;
import fr.eni.cardpostalproject.bo.PostCard;
import fr.eni.cardpostalproject.dao.PostCardDao;

public class AjouterCardActivity extends AppCompatActivity {

    private EditText edtTitle;
    private EditText edtMessage;
    private ImageView image;
    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter_card);

        edtTitle = findViewById(R.id.modifier_txt_title);
        edtMessage = findViewById(R.id.modifier_txt_message);
    }

    public void onClickAjouterCard(View view) {
        PostCardDao dao = new PostCardDao();
        PostCard postCard = new PostCard();

        postCard.setLatitude(MainActivity.latitude);
        postCard.setLongitude(MainActivity.longitude);
        postCard.setTitle(edtTitle.getText().toString());
        postCard.setMessage(edtMessage.getText().toString());
        postCard.setUser(MainActivity.user);

        Media media = new Media();

        dao.insert(this, postCard);

        Intent intent = new Intent(AjouterCardActivity.this, MainActivity.class);
        startActivity(intent);
    }

    public void takePicture(View view) {
        new ImagePicker.Builder(AjouterCardActivity.this)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        image = findViewById(R.id.ajouterCardPicture);
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> mPaths = (List<String>) data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_PATH);
          uri = Uri.parse(mPaths.get(0));
          image.setImageURI(uri);
        }
    }
}
