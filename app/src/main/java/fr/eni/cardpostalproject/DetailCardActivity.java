package fr.eni.cardpostalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import fr.eni.cardpostalproject.bo.PostCard;

public class DetailCardActivity extends AppCompatActivity {

    public static final String EXTRA_CARD = "card";

    private static final String TAG = "DetailCardActivity";

    private PostCard maCard;

    private TextView txtTitle;
    private TextView txtMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_card);

        getSupportActionBar().setHomeButtonEnabled(true);

        Intent intentRecu = getIntent();
        if(intentRecu != null) {
            maCard = intentRecu.getParcelableExtra(EXTRA_CARD);
        }

        txtTitle = findViewById(R.id.detail_txt_title);
        txtMessage = findViewById(R.id.detail_txt_message);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(maCard != null) {
            txtTitle.setText(maCard.getTitle());
            txtMessage.setText(maCard.getMessage());
        }
    }
}
