package fr.eni.cardpostalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import fr.eni.cardpostalproject.bo.User;
import fr.eni.cardpostalproject.dao.UserDao;

public class InscriptionActivity extends AppCompatActivity {

    private EditText edtEmail;
    private EditText edtUsername;
    private EditText edtPassword;
    private EditText edtConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        edtEmail = findViewById(R.id.edit_txt_username);
        edtUsername = findViewById(R.id.edit_txt_username);
        edtPassword = findViewById(R.id.edit_txt_password);
        edtConfirmPassword = findViewById(R.id.edit_txt_confirm_password);
    }

    public void onClickInscription(View view) {
        UserDao dao = new UserDao();
        User user = new User();

        Log.d("InscriptionActivity", "Password : " + edtPassword.getText().toString());
        Log.d("InscriptionActivity", "Confirm Password : " + edtConfirmPassword.getText().toString());

        if (edtPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {
            user.setEmail(edtEmail.getText().toString());
            user.setUsername(edtUsername.getText().toString());
            user.setPassword(edtPassword.getText().toString());
            dao.insert(this, user);

            Intent intent = new Intent(InscriptionActivity.this, LoginActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Les mots de passe ne sont pas identiques", Toast.LENGTH_SHORT).show();
        }
    }
}
