package fr.eni.cardpostalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import fr.eni.cardpostalproject.bo.User;
import fr.eni.cardpostalproject.dao.UserDao;

public class LoginActivity extends AppCompatActivity {

    private EditText edtUsername;
    private EditText edtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtUsername = findViewById(R.id.edit_txt_username);
        edtPassword = findViewById(R.id.edit_txt_password);
    }

    public void onClickConnexion(View view) {
        UserDao dao = new UserDao();
        User user;
        user = dao.getUserByNamePassword(this, edtUsername.getText().toString(), edtPassword.getText().toString());

        if (user.getId() == 0) {
            Toast.makeText(this, "Nom d'utilisateur ou mot de passe incorrect", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            MainActivity.user = user;
            startActivity(intent);
        }
    }

    public void onClickInscription(View view) {
        Intent intent = new Intent(LoginActivity.this, InscriptionActivity.class);
        startActivity(intent);
    }
}
