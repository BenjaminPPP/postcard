package fr.eni.cardpostalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import fr.eni.cardpostalproject.bo.User;
import fr.eni.cardpostalproject.fragments.MapFragment;

public class MainActivity extends AppCompatActivity {

    public static User user;
    public static double latitude;
    public static double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickAjouterCard(View view) {
        MapFragment frag = (MapFragment) getSupportFragmentManager().findFragmentById(R.id.main_frag_map);
        latitude = frag.myLocationoverlay.getMyLocation().getLatitude();
        longitude = frag.myLocationoverlay.getMyLocation().getLongitude();

        Intent intent = new Intent(MainActivity.this, AjouterCardActivity.class);
        startActivity(intent);
    }
}
