package fr.eni.cardpostalproject;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.eni.cardpostalproject.bo.PostCard;

public class PostCardArrayAdapter extends ArrayAdapter<PostCard> {

    public PostCardArrayAdapter(Context context, List<PostCard> postCards) {
        super(context, R.layout.postcard_adapter_layout, postCards);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.postcard_adapter_layout, parent, false);
        }

        TextView txtTitle = convertView.findViewById(R.id.postcard_title);

        PostCard postCardeAAfficher = getItem(position);
        txtTitle.setText(postCardeAAfficher.getTitle());

        return convertView;
    }
}
