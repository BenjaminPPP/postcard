package fr.eni.cardpostalproject.bll;


import android.content.Context;
import fr.eni.cardpostalproject.bo.User;
import fr.eni.cardpostalproject.dao.UserDao;

public class UserService {

    private static UserService instance;

    public static UserService getInstance() {
        if(instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    private UserService() {
    }

    public User getUser(Context context, User user) {

        UserDao userDao = new UserDao();
        User userConnected = userDao.getUserByNamePassword(context, user.getUsername(), user.getPassword());

        return userConnected;
    }



}
