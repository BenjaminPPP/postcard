package fr.eni.cardpostalproject.bo;

public class Media {

    private PostCard postCard;
    private String type;
    private String url;
    // passer l'url en URI et non en String
    private String description;

    public Media() {
    }

    public Media(String idCard, String type, String url, String description) {

        PostCard card = new PostCard();
        card.setId(idCard);

        this.postCard = card;
        this.type = type;
        this.url = url;
        this.description = description;
    }

    public PostCard getPostCard() {
        return postCard;
    }

    public void setPostCard(PostCard postCard) {
        this.postCard = postCard;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
