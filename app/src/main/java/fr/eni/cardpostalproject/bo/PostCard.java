package fr.eni.cardpostalproject.bo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class PostCard implements Parcelable {

    private String id;
    private Double latitude;
    private double longitude;
    private String title;
    private String message;
    private User user;
    private List<Media> medias;

    public PostCard() {
    }

    public PostCard(String id, Double latitude, double longitude, String title, String message) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
        this.message = message;
    }

    public PostCard(String id, Double latitude, double longitude, String title, String message, User user) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
        this.message = message;
        this.user = user;
    }

    public static final Creator<PostCard> CREATOR = new Creator<PostCard>() {

        @Override
        public PostCard createFromParcel(Parcel in) { return new PostCard(in); }
        @Override
        public PostCard[] newArray(int size) { return new PostCard[size]; }

    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(title);
        dest.writeString(message);
    }

    protected PostCard(Parcel in) {
        id = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        title = in.readString();
        message = in.readString();
    }

    @Override
    public String toString() {
        return "PostCards{" +
                "id='" + id + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", users=" + user +
                '}';
    }

    public void ajouterMedia(Media media) {
        this.medias.add(media);
        media.setPostCard(this);
    }

    public List<Media> getMedias() {
        return medias;
    }

    public void setMedias(List<Media> medias) {
        this.medias = medias;
    }
}

