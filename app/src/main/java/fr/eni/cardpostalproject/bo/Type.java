package fr.eni.cardpostalproject.bo;

public enum Type {
    Video(1),
    Music(2),
    Picture(3),
    Sketch(4);

    private int type;

    Type(int type) { this.type = type; }

    public int getType() { return this.type; }


}
