package fr.eni.cardpostalproject.bo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class User implements Parcelable {


    private int id;
    private String username;
    private String token_id;
    private String email;
    private String password;
    private List<PostCard> postCards;

    public User() {
    }

    public User(int id, String username, String token_id, String email, String password) {
        this.id = id;
        this.username = username;
        this.token_id = token_id;
        this.email = email;
        this.password = password;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) { return new User(in); }
        @Override
        public User[] newArray(int size) { return new User[size]; }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<PostCard> getPostCards() {
        return postCards;
    }

    public void setPostCards(List<PostCard> postCards) {
        this.postCards = postCards;
        for(PostCard card : postCards) {
            card.setUser(this);
        }
    }

    public void addPostCard(PostCard card) {
        this.postCards.add(card);
        card.setUser(this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(username);
        dest.writeString(token_id);
        dest.writeString(email);
        dest.writeString(password);
    }

    protected User(Parcel in) {
        id = in.readInt();
        username = in.readString();
        token_id = in.readString();
        email = in.readString();
        password = in.readString();
    }

    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", token_id='" + token_id + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
