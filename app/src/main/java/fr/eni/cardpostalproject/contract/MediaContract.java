package fr.eni.cardpostalproject.contract;

public class MediaContract {

    public static final String TABLE_NAME = "Medias";

    public static final String COL_TYPE = "type";
    public static final String COL_URL = "url";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_POSTCARD = "postcard";

    public static final String CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
            + COL_TYPE + " TEXT NOT NULL,"
            + COL_POSTCARD + " TEXT NOT NULL,"
            + COL_URL + " TEXT NOT NULL,"
            + COL_DESCRIPTION  + " TEXT NULL,"
            + "FOREIGN KEY (" + COL_POSTCARD + ") REFERENCES "
            + PostCardContract.TABLE_NAME + "(" + PostCardContract.COL_ID + ") ON DELETE CASCADE, "
            + "PRIMARY KEY (" + COL_POSTCARD + ", " + COL_TYPE + "))";


    public static final String DROP_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;


}




