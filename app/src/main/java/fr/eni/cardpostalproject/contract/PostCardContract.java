package fr.eni.cardpostalproject.contract;

public class PostCardContract {

    public static final String TABLE_NAME = "PostCards";

    public static final String COL_ID = "id";
    public static final String COL_LAT = "latitude";
    public static final String COL_LONG = "longitude";
    public static final String COL_TITLE = "title";
    public static final String COL_MESSAGE = "message";
    public static final String COL_USER = "user";

    public static final String CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COL_LAT + " DOUBLE NOT NULL,"
            + COL_LONG + " DOUBLE NOT NULL,"
            + COL_TITLE + " TEXT NULL,"
            + COL_MESSAGE + " TEXT NULL,"
            + COL_USER + " INTEGER,"
            + "FOREIGN KEY("+ COL_USER + ") REFERENCES "
            + UserContract.TABLE_NAME + "(" + UserContract.COL_ID +") ON DELETE CASCADE)";

    public static final String DROP_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

}
