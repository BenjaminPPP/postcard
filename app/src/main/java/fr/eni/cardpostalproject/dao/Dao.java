package fr.eni.cardpostalproject.dao;

import android.content.Context;

import java.util.List;

public interface Dao<T> {

    List<T> selectAll(Context context);

    void insert(Context context, T value);

    void update(Context context, T value);

    void delete(Context context, T value);
}
