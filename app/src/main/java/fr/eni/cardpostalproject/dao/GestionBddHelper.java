package fr.eni.cardpostalproject.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import fr.eni.cardpostalproject.bo.PostCard;
import fr.eni.cardpostalproject.contract.MediaContract;
import fr.eni.cardpostalproject.contract.PostCardContract;
import fr.eni.cardpostalproject.contract.UserContract;

public class GestionBddHelper extends SQLiteOpenHelper {

    private static final String DATABASENAME = "post_cards";
    private static final int VERSION = 3;

    GestionBddHelper(Context context) {
        super(context, DATABASENAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(UserContract.CREATE_TABLE);
        db.execSQL(PostCardContract.CREATE_TABLE);
        db.execSQL(MediaContract.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(UserContract.DROP_TABLE);
        db.execSQL(PostCardContract.DROP_TABLE);
        db.execSQL(MediaContract.DROP_TABLE);
        onCreate(db);
    }
}
