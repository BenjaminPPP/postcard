package fr.eni.cardpostalproject.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import fr.eni.cardpostalproject.bo.Media;
import fr.eni.cardpostalproject.bo.PostCard;
import fr.eni.cardpostalproject.contract.MediaContract;
import fr.eni.cardpostalproject.contract.PostCardContract;
import fr.eni.cardpostalproject.contract.UserContract;

public class MediaDao implements Dao<Media> {


    @Override
    public List<Media> selectAll(Context context) {
        return null;
    }

    @Override
    public void insert(Context context, Media media) {

        try(SQLiteDatabase db = new GestionBddHelper(context).getWritableDatabase()) {
            try {
                db.beginTransaction();
                db.insert(UserContract.TABLE_NAME, null, mediaToContentValues(media));

                db.setTransactionSuccessful();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                //si on n'effectue pas l'instruction setTransactionSuccessful avant : rollback
                db.endTransaction();
            }
        }
    }

    @Override
    public void update(Context context, Media value) {

    }

    @Override
    public void delete(Context context, Media value) {

    }

    public static Media map(Cursor cursor) {

        String idCard = cursor.getString(cursor.getColumnIndex(MediaContract.COL_POSTCARD));
        String type = cursor.getString(cursor.getColumnIndex(MediaContract.COL_TYPE));
        String url = cursor.getString(cursor.getColumnIndex(MediaContract.COL_URL));
        String description = cursor.getString(cursor.getColumnIndex(MediaContract.COL_DESCRIPTION));

        return new Media(idCard, type, url, description);
    }


    public static ContentValues mediaToContentValues(Media media) {

        ContentValues values = new ContentValues();

        values.put(MediaContract.COL_POSTCARD, media.getPostCard().getId());
        values.put(MediaContract.COL_TYPE, media.getType().toString());
        values.put(MediaContract.COL_URL, media.getUrl());
        values.put(MediaContract.COL_DESCRIPTION, media.getDescription());

        return values;
    }

}
