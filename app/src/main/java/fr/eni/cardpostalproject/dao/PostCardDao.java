package fr.eni.cardpostalproject.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.eni.cardpostalproject.bo.Media;
import fr.eni.cardpostalproject.bo.PostCard;
import fr.eni.cardpostalproject.bo.User;
import fr.eni.cardpostalproject.contract.MediaContract;
import fr.eni.cardpostalproject.contract.PostCardContract;
import fr.eni.cardpostalproject.contract.UserContract;

public class PostCardDao implements Dao<PostCard> {


    @Override
    public List<PostCard> selectAll(Context context) {

        List<PostCard> cards = new ArrayList<>();

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(PostCardContract.TABLE_NAME + " LEFT JOIN " + MediaContract.TABLE_NAME + " ON "
                + PostCardContract.COL_ID + "=" + MediaContract.COL_POSTCARD);

        try(SQLiteDatabase db = new GestionBddHelper(context).getReadableDatabase()) {
            Cursor cursor = builder.query(db, null, null, null, null, null, null);

            PostCard card;

            while(cursor.moveToNext()) {
                card = map(cursor);
                cards.add(card);
                if (null != cursor.getString(cursor.getColumnIndex(MediaContract.COL_URL))) {
                    card.ajouterMedia(MediaDao.map(cursor));
                }
            }
        }
        return cards;
    }

    @Override
    public void insert(Context context, PostCard postCard) {

        try(SQLiteDatabase db = new GestionBddHelper(context).getWritableDatabase()) {
            try {
                db.beginTransaction();
                long newId = db.insert(PostCardContract.TABLE_NAME, null, postCardToContentValues(postCard));
                postCard.setId(String.valueOf(newId));

                if (null != postCard.getMedias()) {
                    for(Media media : postCard.getMedias()) {
                        db.insert(MediaContract.TABLE_NAME, null, MediaDao.mediaToContentValues(media));
                    }
                }
                db.setTransactionSuccessful();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                //si on n'effectue pas l'instruction setTransactionSuccessful avant : rollback
                db.endTransaction();
            }
        }
    }

    @Override
    public void update(Context context, PostCard card) {
        try(SQLiteDatabase db = new GestionBddHelper(context).getWritableDatabase()) {
            db.update(PostCardContract.TABLE_NAME,
                    postCardToContentValues(card),
                    PostCardContract.COL_ID+"=?",
                    new String[]{
                            String.valueOf(card.getId())
                    }
            );
        }
    }

    @Override
    public void delete(Context context, PostCard card) {
        try(SQLiteDatabase db = new GestionBddHelper(context).getWritableDatabase()) {
            db.delete(PostCardContract.TABLE_NAME, PostCardContract.COL_ID+"=?",
                    new String[]{
                            String.valueOf(card.getId())
                    });
        }
    }

    public static PostCard map(Cursor cursor) {

        String id = cursor.getString(cursor.getColumnIndex(PostCardContract.COL_ID));
        Double latitude = cursor.getDouble(cursor.getColumnIndex(PostCardContract.COL_LAT));
        Double longitude = cursor.getDouble(cursor.getColumnIndex(PostCardContract.COL_LONG));
        String title = cursor.getString(cursor.getColumnIndex(PostCardContract.COL_TITLE));
        String message = cursor.getString(cursor.getColumnIndex(PostCardContract.COL_MESSAGE));

        return new PostCard(id, latitude, longitude, title, message);
    }


    public static ContentValues postCardToContentValues(PostCard card) {

        ContentValues values = new ContentValues();
        values.put(PostCardContract.COL_LAT, card.getLatitude());
        values.put(PostCardContract.COL_LONG, card.getLongitude());
        values.put(PostCardContract.COL_TITLE, card.getTitle());
        values.put(PostCardContract.COL_MESSAGE, card.getMessage());
        values.put(PostCardContract.COL_USER, card.getUser().getId());

        return values;
    }

    /**
     * Récupère les cartes postales selon le type de médias et la carte postale
     * tâche #14
     * @param context
     * @param type
     * @return
     */
    public List<PostCard> selectByTypeAndCard(Context context, PostCard postCard, String type) {

        List<PostCard> cards = new ArrayList<>();

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(PostCardContract.TABLE_NAME + " JOIN " + MediaContract.TABLE_NAME + " ON "
                + PostCardContract.COL_ID + "=" + MediaContract.COL_POSTCARD);

        builder.appendWhere(MediaContract.COL_TYPE + "=" + type + " AND " +
                MediaContract.COL_POSTCARD + "=" + postCard.getId() );

        try(SQLiteDatabase db = new GestionBddHelper(context).getReadableDatabase()) {
            Cursor cursor = builder.query(db, null, null, null, null, null, null);

            PostCard card;

            while(cursor.moveToNext()) {
                card = map(cursor);
                cards.add(card);

                card.ajouterMedia(MediaDao.map(cursor));
            }
        }
        return cards;
    }

    /**
    Map<String, String> map = new HashMap<String, String>();
    map.put("name", "demo");
    map.put("fname", "fdemo");
    // etc

    map.get("name"); // returns "demo"


    public List<PostCard> SelectGeneric(Context context, HashMap<String, String> params) {

        List<PostCard> cards = new ArrayList<>();

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(PostCardContract.TABLE_NAME + " JOIN " + MediaContract.TABLE_NAME + " ON "
                + PostCardContract.COL_ID + "=" + MediaContract.COL_POSTCARD);

        int paramNumber = params.size();

        builder.appendWhere(MediaContract.COL_TYPE + "=" + type + " AND " +
                MediaContract.COL_POSTCARD + "=" + postCard.getId() );

        try(SQLiteDatabase db = new GestionBddHelper(context).getReadableDatabase()) {
            Cursor cursor = builder.query(db, null, null, null, null, null, null);

            PostCard card;

            while(cursor.moveToNext()) {
                card = map(cursor);
                cards.add(card);

                card.ajouterMedia(MediaDao.map(cursor));
            }
        }
        return cards;
    } */



}
