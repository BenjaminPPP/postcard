package fr.eni.cardpostalproject.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import java.util.List;

import fr.eni.cardpostalproject.bo.User;
import fr.eni.cardpostalproject.contract.PostCardContract;
import fr.eni.cardpostalproject.contract.UserContract;

public class UserDao implements Dao<User> {


    @Override
    public List<User> selectAll(Context context) {

        // Not needed for now
        return null;
    }

    @Override
    public void insert(Context context, User user) {

        try(SQLiteDatabase db = new GestionBddHelper(context).getWritableDatabase()) {
            try {
                db.beginTransaction();
                long newId = db.insert(UserContract.TABLE_NAME, null, UserToContentValues(user));
                user.setId((int) newId);

                db.setTransactionSuccessful();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                //si on n'effectue pas l'instruction setTransactionSuccessful avant : rollback
                db.endTransaction();
            }
        }
    }

    @Override
    public void update(Context context, User user) {

        try(SQLiteDatabase db = new GestionBddHelper(context).getWritableDatabase()) {
            db.update(UserContract.TABLE_NAME,
                    UserToContentValues(user),
                    UserContract.COL_ID+"=?",
                    new String[]{
                            String.valueOf(user.getId())
                    }
            );
        }

    }

    @Override
    public void delete(Context context, User user) {

        try(SQLiteDatabase db = new GestionBddHelper(context).getWritableDatabase()) {
            db.delete(UserContract.TABLE_NAME, UserContract.COL_ID+"=?",
                    new String[]{
                            String.valueOf(user.getId())
                    });
        }
    }

    public static User map(Cursor cursor) {

        int id = cursor.getInt(cursor.getColumnIndex(UserContract.COL_ID));
        String username = cursor.getString(cursor.getColumnIndex(UserContract.COL_USERNAME));
        String tokenId = cursor.getString(cursor.getColumnIndex(UserContract.COL_TOKEN_ID));
        String email = cursor.getString(cursor.getColumnIndex(UserContract.COL_EMAIL));
        String password = cursor.getString(cursor.getColumnIndex(UserContract.COL_PASSWORD));

        return new User(id, username, tokenId, email, password);
    }

    public User getUserByNamePassword(Context context, String username, String password) {

        User user = new User();

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(UserContract.TABLE_NAME);
        builder.appendWhere(UserContract.COL_USERNAME + "=" + username + " AND " +
                UserContract.COL_PASSWORD + "=" + password );

        try(SQLiteDatabase db = new GestionBddHelper(context).getReadableDatabase()) {
            Cursor cursor = builder.query(db, null, null, null, null, null, null);

            while(cursor.moveToNext()) {
                user = map(cursor);
            }
        }

        return user;

    }



    public static ContentValues UserToContentValues(User user) {

        ContentValues values = new ContentValues();
        values.put(UserContract.COL_USERNAME, user.getUsername());
        values.put(UserContract.COL_TOKEN_ID, user.getToken_id());
        values.put(UserContract.COL_EMAIL, user.getEmail());
        values.put(UserContract.COL_PASSWORD, user.getPassword());

        return values;
    }

}


