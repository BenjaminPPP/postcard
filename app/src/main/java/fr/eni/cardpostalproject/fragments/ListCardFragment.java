package fr.eni.cardpostalproject.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import fr.eni.cardpostalproject.PostCardArrayAdapter;
import fr.eni.cardpostalproject.R;
import fr.eni.cardpostalproject.bo.PostCard;
import fr.eni.cardpostalproject.dao.PostCardDao;

public class ListCardFragment extends Fragment {

    private List<PostCard> postCards;
    private PostCardArrayAdapter adapter;

    public ListCardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list_card, container, false);

        postCards = new ArrayList<>();
        adapter = new PostCardArrayAdapter(getContext(), postCards);

        final ListView listView = view.findViewById(R.id.main_listview);
        listView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        postCards.clear();
        PostCardDao postCardDao = new PostCardDao();

        postCards.addAll(postCardDao.selectAll(getContext()));
        adapter.notifyDataSetChanged();
    }

}
