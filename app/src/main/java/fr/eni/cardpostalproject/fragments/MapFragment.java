package fr.eni.cardpostalproject.fragments;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.infowindow.InfoWindow;
import org.osmdroid.views.overlay.infowindow.MarkerInfoWindow;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.List;

import fr.eni.cardpostalproject.R;
import fr.eni.cardpostalproject.bo.PostCard;
import fr.eni.cardpostalproject.dao.PostCardDao;

public class MapFragment extends Fragment {

    public MapView map = null;
    public MyLocationNewOverlay myLocationoverlay;
    private List<PostCard> postCards;
    GeoPoint startPoint;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Configuration.getInstance().load(getContext(), PreferenceManager.getDefaultSharedPreferences(getContext()));

        View view = inflater.inflate(R.layout.fragment_map, container, false);

        map = view.findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        map.setUseDataConnection(true);

        IMapController mapController = map.getController();
        mapController.setZoom(18.0);
        startPoint = new GeoPoint(48.0389214, -1.692392400000017);
        mapController.setCenter(startPoint);

        PostCardDao postCardDao = new PostCardDao();
        postCards = new ArrayList<>();
        postCards.addAll(postCardDao.selectAll(getContext()));

        for (PostCard postCard : postCards) {
            startPoint = new GeoPoint(postCard.getLatitude(), postCard.getLongitude());
            Marker startMarker = new Marker(map);
            startMarker.setTitle(postCard.getTitle());
            startMarker.setPosition(startPoint);
            startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            InfoWindow infoWindow = new CustomInfoWindow(map);

            startMarker.setInfoWindow(infoWindow);
            map.getOverlays().add(startMarker);
        }

        myLocationoverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(getContext()), map);
        myLocationoverlay.enableMyLocation();
        map.getOverlays().add(myLocationoverlay);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        map.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        map.onPause();
    }

    public class CustomInfoWindow extends MarkerInfoWindow {
        public CustomInfoWindow(MapView mapView) {
            super(org.osmdroid.bonuspack.R.layout.bonuspack_bubble, mapView);
        }
    }

}
