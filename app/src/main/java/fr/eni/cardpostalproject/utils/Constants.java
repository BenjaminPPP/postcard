package fr.eni.cardpostalproject.utils;

public class Constants {

    private static final String ORIGIN_FILE_PATH = "medias/";
    public static final String MUSIC_PATH = ORIGIN_FILE_PATH + "musiques/";
    public static final String PICTURES_PATH = ORIGIN_FILE_PATH + "pictures/";
    public static final String VIDEOS_PATH = ORIGIN_FILE_PATH + "videos/";
    public static final String SKETCH_PATH = ORIGIN_FILE_PATH + "sketchs/";

}
